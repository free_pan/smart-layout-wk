# 前端项目模板

不同的分支代表不同的项目模板

| 分支名                   | 说明                                       |
| :----------------------- | :----------------------------------------- |
| vue3                     | vue3 项目模板                              |
| vue3-element-plus        | 基于 vue3 模板，集成 element-plus          |
| vue3-vant                | 基于 vue3 模板，集成 vant                  |
| vitepress-doc            | 基于 viteress 的文档模板                   |
| vue3-ui-lib              | 用于编写 vue3 ui 组件                      |
| vue3-element-plus-ui-lib | 用于编写 vue3 ui 组件(引入了 element plus) |

当前是`vitepress-doc`模板，专门用于编写文档

## 已集成组件

- pretieer: 对代码进行格式化
- element-plus: 可以在文档中直接使用 element-plus 组件
- vite-plugin-vitepress-demo: 可以在代码示例下面展示源代码

## script 说明

- **docs:dev**: 本地启动 vitepress 服务
- **docs:build**: 打包
- **docs:preview**: 预览打包结果
- **watchChangeRestartVitepress**: 启动监听器, 监听`articles`文件夹的更新监测，一旦监测到内容改变，则自动触发`vitepress`重启，用于重新生成导航数据

平时写文章的时候，如果需要在新增了文章或修改了文章信息时，自动更新导航数据，那应该，先启动`docs:dev`,再启动`watchChangeRestartVitepress`

## 文件说明

- `docs/vite.confg.ts`: vite 配置文件
- `docs`: 文档目录
- `docs/public`: 静态资源目录
- `docs/.vitepress/config.ts`: 主要用于配置头部导航
- `docs/.vitepress/theme/index.ts`: 主要用于引入全局组件，全局样式等等
- `docs/.vitepress/watchJson.json`: 用于辅助`watchChangeRestartVitepress.mjs`中代码，触发`vitepress`的手动重启
- `docs/.vitepress/watchChangeRestartVitepress.mjs`: 监听`articles`文件夹变化的代码, 用于自动触发`vitepress`的重启
- `docs/.vitepress/navSidebarUtil.ts`: 生成导航数据的代码

## 目录与文章文件约束

> 以下这些约束和`vitepress`无法，是`navSidebarUtil.ts`中的代码实现逻辑，形成的如下约束

- 所有的文章以及文章的目录都必须放在`articles`目录之下
- `articles`下的直属目录是为`一级目录`，该目录用于生成顶部导航
- `一级目录`下只能放`二级目录`以及`index.md`文件。即：`一级目录`下只能放目录和`index.md`, 一级目录下的直属目录属于`二级目录`
- `一级目录`下必须放一个`index.md`文件
- `二级目录`下才是存放你真正文章的`markdown`文件
- 如果需要控制顶部导航的显示顺序，请将`一级目录名`命名为这种风格`01-目录名`,`02-目录名`，即:前 2 位为数字，第三位为`-`的模式
- 如果要控制文章在侧边栏的显示顺序，请将文章命名为这种风格`01-xxx.md`,`02-yyy.md`, 即:前 2 位为数字，第三位为`-`的模式

另外: 所有的`demo目录`以及`asserts目录`都会被自动生成导航的代码排除在外，`demo`目录用于放代码文件,`asserts`用于存放文章中引用的静态资源文件(如:图片)
