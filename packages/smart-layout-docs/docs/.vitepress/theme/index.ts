import { type Theme } from 'vitepress'
import DefaultTheme from 'vitepress/theme'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import { AntdTheme } from '@pzy915/vite-plugin-vitepress-demo/theme'
import 'animate.css'

export default {
  ...DefaultTheme,
  //   NotFound: () => 'custom 404', // <- this is a Vue 3 functional component
  enhanceApp({ app, router, siteData }) {
    // app is the Vue 3 app instance from `createApp()`. router is VitePress'
    // app.use(animated)
    app.use(ElementPlus)
    app.component('Demo', AntdTheme)
    // custom router. `siteData`` is a `ref`` of current site-level metadata.
  },
} as Theme
