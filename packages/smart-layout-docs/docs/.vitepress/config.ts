import { defineConfig } from 'vitepress'
import { getSidebarData, getNavData } from './navSidebarUtil'
import updateInfo from './watchJson.json'

console.log(updateInfo)

export default defineConfig({
  markdown: {
    lineNumbers: true,
  },
  title: '智能布局组件',
  description: '分为普通项智能布局和最后一项是操作项的智能布局',
  appearance: 'dark',
  head: [
    [
      'meta',
      {
        'http-equiv': 'cache-control',
        content: 'no-cache,no-store, must-revalidate',
      },
    ],
    ['meta', { 'http-equiv': 'pragma', content: 'no-cache' }],
    ['meta', { 'http-equiv': 'Expires', content: '0' }],
  ],
  // 如果你计划将你的站点部署到 https://foo.github.io/bar/，那么你应该将 base 设置为 '/bar/'. 默认值：/
  base: '/smart-layout-wk/',
  lang: 'zh-CN',
  outDir: './.vitepress/doc-dist',
  themeConfig: {
    logo: '/logo.svg',
    outlineTitle: '文章目录',
    // 表示文章右侧目录展示h2-h6的内容
    outline: 'deep',
    // nav: [
    //   {
    //     text: 'vue实现动效',
    //     items: [
    //       {
    //         text: 'Transition标签',
    //         link: '/01.vue实现动效/01.Transition标签',
    //       },
    //     ],
    //   },
    // ],
    nav: getNavData({ enableDirActiveMatch: true }),
    sidebar: getSidebarData(),
    // sidebar: {
    //   '/articles/01-作品集/': [
    //     {
    //       text: '工具类',
    //       items: [{text:'xxx',link:'xxx'}],
    //     },
    //     {
    //       text: 'css/scss/less',
    //       items: [],
    //     },
    //     {
    //       text: '移动端',
    //       items: [],
    //     },
    //     {
    //       text: '算法',
    //       items: [],
    //     },
    //     {
    //       text: 'node',
    //       items: [],
    //     },
    //   ],
    // },
  },
})
