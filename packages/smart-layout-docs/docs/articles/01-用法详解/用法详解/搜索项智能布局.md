---
title: 搜索项智能布局
---

# {{$frontmatter.title}}

## 使用场景

搜索表单，每行的表单项数量要根据容器 dom 元素的宽度变化，且最后一个表单项用于控制表单的整体的展开/收缩

## SearchItemSmartLayout props

| 属性名                    | 类型          | 是否必须 | 说明                                    |
| :------------------------ | :------------ | :------- | :-------------------------------------- |
| layoutCalcFun             | LayoutCalcFun | 是       | 布局计算函数                            |
| expand                    | boolean       | 否       | 当前是否处于展开状态. 默认:false        |
| lastItemFlotRight         | boolean       | 否       | 最后一项(li)是否浮动到最右侧. 默认:true |
| alwaysShowLastOperateItem | boolean       | 否       | 是否一直显示最后一个操作项. 默认:true   |

```ts
/**
 * 布局计算函数定义
 * @param smartLayoutContainerDom 布局管理器的父容器dom
 * @return 在当前父容器dom下，每行应该容纳的li数量
 */
export type LayoutCalcFun = (smartLayoutContainerDom: HTMLElement) => number
```

## SmartLayoutItem props

| 属性名        | 类型    | 是否必须 | 说明                                                                                                                                                                                                                                                                                                                     |
| :------------ | :------ | :------- | :----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| isOperateItem | boolean | 否       | 表示当前项是否操作项. 默认:false<br/><br/>只在 `SearchItemSmartLayout` 下生效，且设置 `isOperateItem` 为 `true` 的项必须在所有 `SmartLayoutItem` 之后，且在同一个布局器下只能有一个这样的 `SmartLayoutItem`<br/><br/>操作项的意思是该 `SmartLayoutItem` 中包含控制 `SearchItemSmartLayout expand` 属性改变的 `html` 元素 |

## 使用方式

```vue
<script setup lang="ts">
import { SmartLayoutItem, SearchItemSmartLayout } from '@pzy915/smart-layout'
import '@pzy915/smart-layout/dist/style.css'

function layoutCalcFun(smartLayoutDom: HTMLElement) {
  const offsetWidth = smartLayoutDom.offsetWidth
  // 宽度大于等于634则每行显示3列
  if (offsetWidth >= 634) return 3
  // 宽度大于等于307则每行显示2列
  if (offsetWidth >= 307) return 2
  // 否则显示1列
  return 1
}
const expand = ref<boolean>(false)
</script>

<template>
  <SearchItemSmartLayout :layout-calc-fun="layoutCalcFun" :expand="expand">
    <SmartLayoutItem>
      <el-input :placeholder="`input:第1项...`"></el-input>
    </SmartLayoutItem>
    <SmartLayoutItem :is-operate-item="true">
      <el-button @click="expand = !expand">展开/收缩</el-button>
    </SmartLayoutItem>
  </SearchItemSmartLayout>
</template>
```

## 代码示例

<demo src="./demo/SearchItemSmartLayoutDemo.vue" title="说明" desc="该示例的最后一项会一直浮动在最右侧"></demo>

<demo src="./demo/SearchItemSmartLayoutDemo02.vue" title="说明" desc="该示例的最后一项会紧随其他项之后，不会浮动到最右侧"></demo>

<demo src="./demo/SearchItemSmartLayoutDemo03.vue" title="说明" desc="没有设置操作项目的情况下，则强制显示所有项"></demo>

<demo src="./demo/SearchItemSmartLayoutDemo04.vue" title="非操作li的边界数量测试" desc="非操作li数量，刚好等于一行显示的li数量，则操作li无需显示" :otherSrcArr="['./demo/MyButton.vue']"></demo>

<demo src="./demo/SearchItemSmartLayoutDemo05.vue" title="非操作li的边界数量测试" desc="隐藏第三和第四个li后，则剩余的非操作li，可以在一行显示完成，则操作li无需显示" :otherSrcArr="['./demo/MyButton.vue']"></demo>

<demo src="./demo/SearchItemSmartLayoutDemo06.vue" title="非操作li的边界数量测试" desc="每行显示一个，且只有一个表单项"  :otherSrcArr="['./demo/MyButton.vue']"></demo>

<demo src="./demo/SearchItemSmartLayoutDemo07.vue" title="非操作li的边界数量测试" desc="每行显示三个，但只有两个表单项"  :otherSrcArr="['./demo/MyButton.vue']"></demo>
