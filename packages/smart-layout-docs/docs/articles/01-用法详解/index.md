---
title: 安装与用法详解
---

# {{$frontmatter.title}}

## 安装

```
pnpm add @pzy915/smart-layout
```

## 项目中引用

```vue
<script>
import { SmartLayoutItem, SearchItemSmartLayout } from '@pzy915/smart-layout'
import '@pzy915/smart-layout/dist/style.css'
</script>
```

## 实现原理

基于`ResizeObserver`动态监听 dom 元素大小变化，然后动态设置 项 的宽度

## 这个组件的智能布局是指什么?

根据智能布局组件所在父容器组件的宽度，动态改变每行能显示的项数量。

分为两种情况的智能布局。

1. 最后一项是非操作项的（普通项智能布局）
2. 最后一项是操作的项的（搜索项智能布局）
