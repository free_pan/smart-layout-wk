---
layout: home

hero:
  name: 智能响应式布局组件
  text: 分为普通项智能布局和最后一项是操作项的智能布局
  tagline: 基于vue3, 仅支持现代浏览器，简洁、好用
  image:
    src: /logo.png
    alt: 智能响应式布局组件
  actions:
    - theme: brand
      text: 开始
      link: /articles/01-用法详解/
    - theme: alt
      text: 获取源代码
      link: https://gitee.com/free_pan/smart-layout-wk.git
    - theme: alt
      text: 获取npm包
      link: https://www.npmjs.com/package/@pzy915/smart-layout

features:
  - icon: ⚡️
    title: 让响应式布局更智能
    details: 试试就知道了
  - icon: 📦
    title: 让工作更轻松
    details: 不是为了摸鱼，而是为了能将时间花在更有意义的事情上
  - icon: 🛠️
    title: 除了vue3，再无任何其他依赖
    details: 紧随最新技术栈
---
