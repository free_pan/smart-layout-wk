import VitePluginVitepressDemo from '@pzy915/vite-plugin-vitepress-demo'
import { defineConfig } from 'vite'
import vueJsx from '@vitejs/plugin-vue-jsx'

export default defineConfig({
  plugins: [vueJsx(), VitePluginVitepressDemo({ glob: './**/demo/**/*.{vue,jsx,tsx,js,ts}' })],
  server: {
    // open: true,
    host: '0.0.0.0',
  },
})
