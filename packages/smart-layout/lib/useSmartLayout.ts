import { inject, InjectionKey, Ref } from 'vue'

export const openCloseBtnNeedShowInjectionKey = Symbol() as InjectionKey<
  Readonly<Ref<boolean>>
>

/**
 * openCloseBtnNeedShow：展开/收缩按钮是否需要显示
 */
export function useSearchItemSmartLayout() {
  /**
   * 展开/收缩按钮是否需要显示
   *
   */
  const openCloseBtnNeedShow = inject(openCloseBtnNeedShowInjectionKey)
  return { openCloseBtnNeedShow }
}
