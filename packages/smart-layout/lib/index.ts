import { App } from 'vue'
import ItemSmartLayout from './ItemSmartLayout.vue'
import SearchItemSmartLayout from './SearchItemSmartLayout.vue'
import SmartLayoutItem from './SmartLayoutItem.vue'
import { useSearchItemSmartLayout } from './useSmartLayout'

export { ItemSmartLayout, SearchItemSmartLayout, SmartLayoutItem }
export { useSearchItemSmartLayout }

export default {
  install(app: App) {
    app.component(ItemSmartLayout.name, ItemSmartLayout)
    app.component(SearchItemSmartLayout.name, SearchItemSmartLayout)
    app.component(SmartLayoutItem.name, SmartLayoutItem)
  },
}
