/**
 * 布局计算函数定义
 * @param smartLayoutContainerDom 布局管理器的父容器dom
 * @return 在当前父容器dom下，每行应该容纳的li数量
 */
export type LayoutCalcFun = (smartLayoutContainerDom: HTMLElement) => number
