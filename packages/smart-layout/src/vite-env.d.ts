/// <reference types="vite/client" />

declare module '*.vue' {
  import type { DefineComponent } from 'vue'
  const component: DefineComponent<
    Record<string, unknown>,
    Record<string, unknown>,
    any
  >
  export default component
}

/**
 * env配置文件boolean值类型
 */
type ENV_BOOLEAN_VALUE_TYPE = 'true' | 'false' | '' | undefined
// 这个ImportMetaEnv的类型声明的作用，是为了让我们在使用自定义的环境变量`import.meta.env.XXX`时，让ts能够自动推导出对应的变量名和变量类型
// eslint-disable-next-line no-unused-vars
interface ImportMetaEnv {
  VITE_HELLO: string
  /**
   * 是否进行包分析
   */
  VITE_PACKAGE_ANALYZER: ENV_BOOLEAN_VALUE_TYPE
}
