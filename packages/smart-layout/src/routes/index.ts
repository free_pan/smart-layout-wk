import {
  type RouteRecordRaw,
  createRouter,
  createWebHashHistory,
} from 'vue-router'
import Page404 from '@/pages/Page404.vue'
import BasicLayou from '@/layouts/BasicLayout.vue'

const routes: RouteRecordRaw[] = [
  // {
  //   path: '/',
  //   redirect: '/home',
  // },
  {
    name: 'Index',
    path: '/',
    component: BasicLayou,
    redirect: 'home',
    children: [
      {
        name: 'Home',
        path: 'home',
        component: () => import('@/pages/Home.vue'),
      },
    ],
  },
  // 将匹配所有内容并将其放在 `$route.params.pathMatch` 下
  { path: '/:pathMatch(.*)*', name: 'NotFound', component: Page404 },
  // 将匹配以 `/user-` 开头的所有内容，并将其放在 `$route.params.afterUser` 下
  // { path: '/user-:afterUser(.*)', component: UserGeneric },
]
const router = createRouter({
  // 4. 内部提供了 history 模式的实现。为了简单起见，我们在这里使用 hash 模式。
  history: createWebHashHistory(),
  routes, // `routes: routes` 的缩写
})

export default router
