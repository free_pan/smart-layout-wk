import { createApp } from 'vue'
import 'modern-normalize/modern-normalize.css'
import App from '@/App.vue'
import 'virtual:svg-icons-register'
import '@/assets/scss/index.scss'
import { createPinia } from 'pinia'
import router from '@/routes/index'
import piniaPersist from 'pinia-plugin-persist'

const app = createApp(App)
const pinia = createPinia()
// piniaPersist使用详见: https://juejin.cn/post/7139002338554937352
pinia.use(piniaPersist)

app.use(pinia)
app.use(router)
app.mount('#app')
