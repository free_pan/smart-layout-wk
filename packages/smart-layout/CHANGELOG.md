# @pzy915/smart-layout 
## 更新历史
### [0.0.5](https://gitee.com/free_pan/smart-layout-wk/compare/v0.0.4...v0.0.5) (2023-03-14)


### 🤖 Chore | 构建/工程依赖/工具

* 🤖 重新打包 ([185b096](https://gitee.com/free_pan/smart-layout-wk/tree/185b0967d14d5bf119ef4cb9fd4da07ff48301fe))


### 🐛 Bug Fixes | Bug 修复

* 🐛 修复显示/隐藏控制逻辑bug ([fb67d51](https://gitee.com/free_pan/smart-layout-wk/tree/fb67d5192eff6c6ab7832f1ca22f895cb7fcfcdc))

### [0.0.4](https://gitee.com/free_pan/smart-layout-wk/compare/v0.0.3...v0.0.4) (2023-03-13)


### 🤖 Chore | 构建/工程依赖/工具

* 🤖 完善示例，重新打包 ([4eb87b9](https://gitee.com/free_pan/smart-layout-wk/tree/4eb87b90a893cc81b15a7bd9fbeee2edd91e9df2))
* 🤖 文档重新打包 ([0f95a75](https://gitee.com/free_pan/smart-layout-wk/tree/0f95a75eb6c432082bbd0204f96ac426aae1ad35))

### [0.0.3](https://gitee.com/free_pan/smart-layout-wk/compare/v0.0.2...v0.0.3) (2023-03-13)


### 🐛 Bug Fixes | Bug 修复

* 🐛 修复只有一个表单项时，以及表单项少于每行显示个数时的问题 ([a81dba1](https://gitee.com/free_pan/smart-layout-wk/tree/a81dba15d520f7f52bc1860c0cbeaf1ab0c82353))


### 🤖 Chore | 构建/工程依赖/工具

* 🤖 重新打包 ([6ef60fa](https://gitee.com/free_pan/smart-layout-wk/tree/6ef60facd974220deb5470ce4a21c36f7dfa4a17))


### ✨ Features | 新功能

* ✨ 在上下文增加openCloseBtnNeedShow变量，用于给外部判断展开/收缩按钮是否需要显示 ([3b6ec9b](https://gitee.com/free_pan/smart-layout-wk/tree/3b6ec9ba8af4bfc24dc91c89316bee04e2c6d15e))

### [0.0.2](https://gitee.com/free_pan/smart-layout-wk/compare/v0.0.1...v0.0.2) (2023-03-10)


### 🤖 Chore | 构建/工程依赖/工具

* 🤖 完善changelog生成 ([0d584cc](https://gitee.com/free_pan/smart-layout-wk/tree/0d584cc54cb9b3aa32ee46823a2c2bd4a6d31e70))


### 📝 Documentation | 文档更新

* 📝 完善README.md文档 ([f09cdfd](https://gitee.com/free_pan/smart-layout-wk/tree/f09cdfdc8b69f047ecd8837f5159e954d2ff3ddf))
* 📝 重新打包 ([3fddb51](https://gitee.com/free_pan/smart-layout-wk/tree/3fddb51105cfedcbcf8533d55ca854312bb0145e))

### 0.0.1 (2023-01-29)
