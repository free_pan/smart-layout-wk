module.exports = {
  header: '# 项目 \n## 更新历史',
  commitUrlFormat: 'http://git项目地址/tree/{{hash}}',
  issueUrlFormat: 'http://gitissu地址{{id}}',
  types: [
    { type: 'feat', section: '✨ Features | 新功能' },
    { type: 'fix', section: '🐛 Bug Fixes | Bug 修复' },
    { type: 'docs', section: '📝 Documentation | 文档更新' },
    { type: 'refactor', section: '♻️ Code Refactoring | 代码重构' },
    { type: 'perf', section: '⚡ Performance Improvements | 性能优化' },
    { type: 'test', section: '✅ Tests | 添加/更新测试代码' },
    { type: 'revert', section: '⏪ Revert | 回退', hidden: true },
    { type: 'release', section: '🔖 Build System | 打包构建' },
    { type: 'chore', section: '🤖 Chore | 构建/工程依赖/工具' },
    { type: 'ci', section: '👷 Continuous Integration | CI 配置' },
  ],
}
